//
//  ContainerView.m
//  SearchTest
//
//  Created by Igor Sutton on 8/7/13.
//  Copyright (c) 2013 Igor Sutton. All rights reserved.
//

#import "SearchBarContainerView.h"

@implementation SearchBarContainerView

- (id)initWithFrame:(CGRect)frame
{
    _stickyFrame = CGRectNull;

    if (!(self = [super initWithFrame:frame]))
    {
        return nil;
    }

    return self;
}

- (void)setFrame:(CGRect)frame
{
    if (CGRectIsNull(_stickyFrame))
    {
        [super setFrame:frame];
    }
    else
    {
        [super setFrame:_stickyFrame];
        self.stickyFrame = CGRectNull;
    }
}

@end
