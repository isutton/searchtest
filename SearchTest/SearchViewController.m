//
//  SearchViewController.m
//  SearchTest
//
//  Created by Igor Sutton on 8/6/13.
//  Copyright (c) 2013 Igor Sutton. All rights reserved.
//

#import "SearchViewController.h"
#import "SearchTableView.h"
#import "SearchBarContainerView.h"

@interface SearchViewController () <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchDisplayDelegate>

@property (nonatomic, strong) SearchTableView *tableView;

@end

@implementation SearchViewController
{
    SearchBarContainerView *_searchBarContainerView;
    UISearchBar *_searchBar;
    UISearchDisplayController *_searchDisplayController;
}

- (void)loadView
{
    [super loadView];

    _searchBar = [[UISearchBar alloc] init];
    [_searchBar sizeToFit];

    _searchDisplayController = [[UISearchDisplayController alloc] initWithSearchBar:_searchBar contentsController:self];
    _searchDisplayController.delegate = self;

    _searchBarContainerView = [[SearchBarContainerView alloc] initWithFrame:_searchBar.bounds];
    [_searchBarContainerView addSubview:_searchBar];

    CGRect tableViewFrame = self.view.bounds;
    self.tableView = [[SearchTableView alloc] initWithFrame:tableViewFrame style:UITableViewStylePlain];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableHeaderView = _searchBarContainerView;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(44.0f, 0.0f, 0.0f, 0.0f);
    [self.view addSubview:self.tableView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [NSString stringWithFormat:@"Section %i", section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];

    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }

    cell.textLabel.text = [NSString stringWithFormat:@"Row %i", indexPath.row];

    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGPoint contentOffset = scrollView.contentOffset;

    if (contentOffset.y > 0.0f)
    {
        scrollView.contentInset = UIEdgeInsetsMake(CGRectGetHeight(_searchBarContainerView.frame), 0.0f, 0.0f, 0.0f);
    }
    else
    {
        scrollView.contentInset = UIEdgeInsetsZero;
    }
}

- (void)searchDisplayControllerDidBeginSearch:(UISearchDisplayController *)controller
{
    _searchBarContainerView.stickyFrame = _searchBarContainerView.frame;
}


@end
