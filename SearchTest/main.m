//
//  main.m
//  SearchTest
//
//  Created by Igor Sutton on 8/6/13.
//  Copyright (c) 2013 Igor Sutton. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
