//
//  AppDelegate.h
//  SearchTest
//
//  Created by Igor Sutton on 8/6/13.
//  Copyright (c) 2013 Igor Sutton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
