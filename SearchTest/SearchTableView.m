//
//  SearchTableView.m
//  SearchTest
//
//  Created by Igor Sutton on 8/6/13.
//  Copyright (c) 2013 Igor Sutton. All rights reserved.
//

#import "SearchTableView.h"

@implementation SearchTableView

- (void)layoutSubviews
{
    [super layoutSubviews];

    if (self.contentOffset.y > 0.0f)
    {
        CGRect frame = self.tableHeaderView.frame;
        frame.origin = self.contentOffset;
        self.tableHeaderView.frame = frame;
    }

    self.tableHeaderView.layer.zPosition = CGFLOAT_MAX;
}

@end
